# Ask Urza: The Mastodon Scryfall Lookup Bot

Ask Urza is a Mastodon bot that provides scryfall lookups much like the Reddit
Card Fetcher Bot and various Discord and Slack bots.

It has two modes, a mention mode, where it will respond specifically to posts
where it is mentioned, and a follow mode, where users can tell the bot to follow
them and jump into conversations when cards are mentioned.


## Installation

 * Clone the repository and `cd` into it. The repo directory will be your working directory
 * Run `wget https://archive.scryfall.com/json/scryfall-oracle-cards.json` to get the current card database
 * Copy `askurza.json` to `/etc/askurza.json`
 * Edit `/etc/askurza.json` with the correct configuration values
 * Copy `askurza.service` to `/etc/systemd/system/askurza.service`
 * Edit `/etc/systemd/system/askurza.service` and replace `USERNAME` with the correct values
    * `User=USERNAME`
    * `Group=USERNAME`
    * `WorkingDirectory=/home/USERNAME/askurza/`
    * `ExecStart=/home/USERNAME/askurza/askurza.py`
 * Add the following lines to your crontab, and replace `USERNAME` with the correct values

```
15 4 * * 0      USERNAME (cd /home/USERNAME/askurza &&  wget `curl https://api.scryfall.com/bulk-data | jq .data[0].download_uri -r` -O new.json && mv new.json scryfall-oracle-cards.json)
15 5 * * 0      root  (service askurza restart)
```

 * Run `pip install Mastodon.py`
 * Run `sudo systemctl enable askurza`
 * Run `sudo systemctl restart askurza`


## Usage

Any time the bot receives a toot in its incoming feed, it will scan the toot for
card names enclosed in `[[double brackets]]`. If it finds any, it will reply to
the author using the same visibility rules with links to the cards' scryfall
pages, and embedded images with the cards' oracle text as the image description.

Any user can mention the bot to have their cards linked, but it's more convenient
to send the bot the `+register` command, which will tell the bot to follow the
user and automatically scan their toots without having to explicitly mention it.
The user can request to no longer be followed by using the `+unregister` command.

## Answers to Frequently Asked Questions

 * You do NOT need to follow the bot for it to work
 * Capitalization does not matter, but you need to specify the full English name of the card with punctuation
 * For double-faced/split cards you need to specify both halves with spaces around the slashes `[[front // back]]`
 * Does not support filtering for particular printings/artworks
