# Contributing to Ask Urza

Ask Urza is a small project so I'm not going to put a whole lot of process here.
The flagship bot lives at [https://botsin.space/@askurza](@askurza), so if you
if you notice any recurring bugs or have any feature requests you would like to
formally request, feel free to open an issue and let me know. I'll try to
accomodate them.

## Bot Support and Bug Reports

If the bot is not working, either not responding at all or returning the wrong
results, please copy and paste or link the toots that you sent it, its responses
(or lack thereof), and what you expected.

## Feature Requests

If you have suggestions for ways for the bot to be more useful, please try to
be thorough as to what you would like changed and why. If it is a substantial
change to the experience, I may invite the bots' users to vote or otherwise
comment on the feature before implementing it. 
