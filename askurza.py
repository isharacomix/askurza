#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pip3 install Mastodon.py wget

import time, os, sys, json
import wget
from mastodon import Mastodon, StreamListener

config = json.loads(open('/etc/askurza.json').read())
your_instance = config['your_instance']
login = config['login']
pwd = config['pwd']
tmpdir = 'tmp'
owner = config['owner']

tmpdir = 'tmp'
if not os.path.exists(tmpdir):
    os.mkdir(tmpdir)

# wget https://archive.scryfall.com/json/scryfall-oracle-cards.json
cards = json.loads(open("scryfall-oracle-cards.json").read())
index = {}
for card in cards:
  index[card['name'].lower()] = card

if not os.path.exists('scryfall_clientcred.txt'):
    Mastodon.create_app(
        'scryfall',
        to_file = 'scryfall_clientcred.txt',
        api_base_url = your_instance
    )

mastodon = Mastodon(
    client_id = 'scryfall_clientcred.txt',
    api_base_url = your_instance
)


def handle(toot):
    cards = fetch_cards(toot)
    if not cards:
        return None, None
    replytext = "\n"
    images = []
    for card in cards[:4]:
      replytext += "%s: %s\n"%(card['name'][:30], card['scryfall_uri'])

      card_file = tmpdir + '/' + card['id'] + '.jpg'
      if not os.path.exists(card_file):
        try:
          wget.download(card['image_uris']['normal'], card_file)
        except:
          card_file = ""
      card_text = "%s: %s"%(card['name'][:30], card.get('oracle_text', "Could not fetch oracle text.")[:300])
      if card_file:
        images.append([card_file, card_text])
    return replytext, images

def handle_commands(status):
    id = status['account']['id']
    username = status['account']['acct']
    banned = abuse_check(id)
    if banned:
        return
    if "+register" in status['content']:
        mastodon.status_post(status="@"+username+" OK! I'm going to send you a follow request. From now on, if you post a card name [[like this]] even without mentioning me, I will reply with the scryfall results. If you want me to stop, just tell me to +unregister.", visibility="direct")
        mastodon.account_follow(id)
    elif "+unregister" in status['content']:
        a= mastodon.status_post(status="@"+username+" OK! I will stop following you and stop replying to your posts. You can still explicitly request card lookups by mentioning me, and can +register later if you change your mind.", visibility="direct")
        b= mastodon.account_unfollow(id)

def fetch_cards(string):
    cardlist = string.lower().split('[[')
    result = []
    for card in cardlist:
        card = card.split(']]')[0]
        if card in index:
          result.append(index[card])
    return result[:10]

history = {}
timers = {}
grace = 120
limit = 100
def abuse_check(id):
    x = mastodon.account_relationships(id)[0]
    if x['blocking']: return True
    if id not in timers: timers[id] = time.time()
    if time.time() - timers[id] > grace:
        timers.pop(id)
        history.pop(id)
    if id not in history: history[id] = 0
    history[id] += 1
    if history[id] > limit:
        mastodon.account_block(id)
        return True
    else:
        pass #mastodon.account_follow(id)
    return False

def check_cards(notification):
        if notification['reblog']:
          return
        reply_text, images = handle(notification['content'])
        if reply_text:
          banned = abuse_check(notification['account']['id'])
          if banned:
             return

          pictures_ids = []
          for i, t in images:
            pictures_ids.append( mastodon.media_post(i, description=t, focus=(-1,1)) )
          mastodon.status_reply(notification, reply_text, media_ids=pictures_ids, untag=True, sensitive=True)

class Listener(StreamListener):
    def on_notification(self, notification):
      if notification['type'] == "mention":
         handle_commands(notification['status'])
         check_cards(notification['status'])
    def on_update(self, status):
      x = mastodon.account_relationships(status['account']['id'])[0]
      if x['following']:
        check_cards(status)

mastodon.log_in(login, pwd)
mastodon.status_post(status="%s Rebooting - loaded %d cards"%(owner, len(index.keys())), visibility="direct")
mastodon.stream_user(Listener())
